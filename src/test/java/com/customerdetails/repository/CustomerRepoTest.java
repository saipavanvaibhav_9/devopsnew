package com.customerdetails.repository;
import static org.junit.Assert.assertFalse;


import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;


import com.customerdetails.customer.entity.Customer;
import com.customerdetails.service.CustomerServiceimpl;
@DataJpaTest
public class CustomerRepoTest {

	 @Autowired
	   CustomerRepo custRepo;
	 @BeforeEach
		public void setup() {
			MockitoAnnotations.openMocks(this);
		}
	 @Test 
	 public void Validrepo() {
	     String customerName = "vaibhav";
	   
	     Customer customer = new Customer();
	     
	     customer.setCustomerName(customerName);
	     custRepo.save(customer);
	       String expectedValue = "vaibhav";
	      
	     Optional<Customer> optionalCustomer = custRepo.findByCustomerName(customerName);

         assertTrue(optionalCustomer.isPresent());
	     String actualvalue=optionalCustomer.get().getCustomerName();
	     assertEquals(expectedValue,actualvalue);
	 }


  
}
