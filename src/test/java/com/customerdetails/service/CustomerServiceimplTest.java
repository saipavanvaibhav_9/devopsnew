package com.customerdetails.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.customerdetails.customer.entity.Customer;
import com.customerdetails.repository.CustomerRepo;

public class CustomerServiceimplTest {

	
	@Mock
	CustomerRepo custRepo;

	@InjectMocks
	private CustomerServiceimpl customerservice;
	
	@BeforeEach
	public void setup() {
		MockitoAnnotations.openMocks(this);
	}
	@Test
	public void Validcustomer(){
		String customerName = "vaibhav";
		String password = "password";
		Customer customer = new Customer();
		customer.setCustomerName(customerName);
		customer.setPassword(password);
		String Exceptedresult = "Login Successful";
		when(custRepo.findByCustomerName(customerName)).thenReturn(Optional.of(customer));
		String actualresult = customerservice.login(customerName, password);
		assertEquals(Exceptedresult,actualresult);
		
		
	}
	@Test
	public void invalidcustomer(){
		  //
			String customerName = "vai";
			String Exceptedresult = "customer not found";
			Customer customer = new Customer();
			when(custRepo.findByCustomerName(customerName)).thenReturn(Optional.of(customer));
			String actualresult = customerservice.login("vaibhav","password");
			assertEquals(Exceptedresult,actualresult);
			
			
//			Optional<Customer> optionalCustomer = custRepo.findByCustomerName(customerName);
//			boolean logic = optionalCustomer.isPresent();
//			if(!logic) {
//				String actualResult="customer not found";
//			
//			
//			assertEquals(Exceptedresult,actualResult);
//			}
//			
			
		}
	
	@Test
	public void wrongpassword(){

			Customer customer = new Customer("vaibhav","password");
			
			String Exceptedresult = "Incorrect Password";
			when(custRepo.findByCustomerName("vaibhav")).thenReturn(Optional.of(customer));
			String actualresult = customerservice.login("vaibhav", "pass");
			assertEquals(Exceptedresult,actualresult);
			
			
		}
	@Test
	public void Emptypassword(){
		  //
			String customerName = "vaibhav";
			String password = "";
			Customer customer = new Customer();
			String Exceptedresult = "customer_name or password can't be empty";
			when(custRepo.findByCustomerName(customerName)).thenReturn(Optional.of(customer));
			String actualresult = customerservice.login(customerName, password);
			assertEquals(Exceptedresult,actualresult);
			
			
		}
	

}
