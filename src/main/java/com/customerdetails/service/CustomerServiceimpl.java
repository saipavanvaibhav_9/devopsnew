package com.customerdetails.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.customerdetails.customer.entity.Customer;
import com.customerdetails.repository.CustomerRepo;

@Service
public class CustomerServiceimpl implements CustomerService {

	@Autowired
	CustomerRepo custRepo;

	public String login(String customer_name, String password) {
//		System.out.println("service");
		if(password.isEmpty()) {
			return("customer_name or password can't be empty");
		}
		Optional<Customer> optionalCustomer = custRepo.findByCustomerName(customer_name);
	
		
		
		if(!optionalCustomer.isPresent()) {
			return("customer not found");
		}
		else {
		
		Customer customer = optionalCustomer.get();
		
		String Pass = customer.getPassword();
		
		

		if (password.equals(Pass)) {
			return "Login Successful";
		}
		return "Incorrect Password";
		}
	}
  }

