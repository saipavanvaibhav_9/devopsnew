	package com.customerdetails.controller;
	
	import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
	import org.springframework.web.bind.annotation.PathVariable;
	import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
	
	import com.customerdetails.customer.entity.Customer;
	import com.customerdetails.service.CustomerServiceimpl;
	
	@RestController
	
	@RequestMapping("/customer")
	@CrossOrigin
	public class CustomerController {
	   
		@Autowired
		CustomerServiceimpl cService;
	
		@GetMapping("/{customer_name}")
		@CrossOrigin
		public String login(@PathVariable("customer_name") String customer_name, @RequestParam("password") String password) {
//			System.out.println("contoller");
			
		
			return cService.login(customer_name, password);
			
			
		}
	        
	}
